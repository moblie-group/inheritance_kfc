import 'dart:collection';
import 'dart:mirrors';

void main(List<String> arguments) {
  var chicken = Chicken();
  var bonelesschicken = BonelessChicken();
  var frenchfries = Frenchfries();
  var nugets = Nugets();
  var hamberger = Hamberger();
  var ptt = Potato();
  var sauce = sauces();
  print('>> KFC food types <<');
  print("-------------------------------------------------------- ");
  chicken.chicken();
  bonelesschicken.bonelesschicken();
  frenchfries.frenchfries();
  nugets.nugets();
  hamberger.display();
  hamberger.vegetable();
  hamberger.vagetableno();
  hamberger.ytmt();
  hamberger.ntmt();
  ptt.display();
  ptt.mashed();
  sauce.display();
  sauce.stmt();
  sauce.schili();
}

abstract class KFC {
  void fry() {
    print('Fry');
  }

  void spicy() {
    print('Spicy');
  }

  void sizeffs() {
    print('Size S');
  }

  void sizeffm() {
    print('Size M');
  }

  void sizeffl() {
    print('Size L');
  }

  void sizeffxl() {
    print('Size XL');
  }

  void chicken() {
    print('*** Chicken ***');
    fry();
    spicy();
  }

  void bonelesschicken() {
    print('*** BonelessChicken ***');
    fry();
    spicy();
  }

  void frenchfries() {
    print('*** Frenchfries ***');
    fry();
    spicy();
    sizeffs();
    sizeffm();
    sizeffl();
    sizeffxl();
  }

  void nugets() {
    print('*** Nugets');
    fry();
    spicy();
  }
}

mixin vegetables {
  void vegetable();
  void vagetableno();
}

mixin sizeham {
  void size1() => print('Size Normal');
  void size2() => print('Size Big');
  void size3() => print('Size Jumbo');
}
mixin tmt {
  void ytmt();
  void ntmt();
}
mixin mash{
  void mashed();
}
mixin sauce{
  void stmt();
  void schili ();
}




class Chicken extends KFC {}

class BonelessChicken extends KFC {}

class Frenchfries extends KFC {}

class Nugets extends KFC {}

class Hamberger extends KFC with sizeham implements vegetables, tmt {
  void display() {
    print('*** Hamberger ***');
    size1();
    size2();
    size3();
  }

  @override
  void vagetableno() {
    print('without vagetable');
  }

  @override
  void vegetable() {
    print('with vagetable');
  }

  @override
  void ntmt() {
    print('without tomatoes');
  }

  @override
  void ytmt() {
    print('with tomatoes');
  }
}

class Potato implements mash{
  void display(){
  print('*** Potato ***');
  }
  @override
  void mashed() {
    print('Mash');
  }

}
class sauces implements sauce{
  void display(){
    print('*** Sauce ***');
  }
  @override
  void schili() {
    print('Chili');
  }

  @override
  void stmt() {
    print('Tometo');
  }

}
